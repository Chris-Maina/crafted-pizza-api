# Crafted-Pizza API
This is a backend service that fleshes out CRUD operations of categories, products, orders, customers and deliveries.

## Technologies used
* GraphQL
* Sequelize
* Postgresql
* Typescript

### Getting started
Clone this repository

#### Install dependencies
Run the code below on your terminal to install dependencies
            `yarn install`

#### Run the application  
            `yarn dev`

Make queries and mutation on the graphiQL playground i.e. `localhost:4000/` to see it in action
