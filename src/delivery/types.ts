export default `
  extend type Query {
    getDelivery(id: Int): Delivery
  }
  extend type Mutation {
    createDelivery(params: DeliveryInput): Delivery
  }
  type Delivery {
    id: Int,
    type: String,
    cost: String,
    time: String,
    location: String,
  }

  input DeliveryInput {
    cost: Int,
    type: String!,
    time: String,
    location: String,
  }
`;