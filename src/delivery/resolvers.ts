import { ID } from '../common';
import { DeliveryAttributes } from "../db/models/delivery";

const getDelivery = async (_:any, { id}: ID, {models}: any) => (
  await models.Delivery.findByPk(id)
);

const createDelivery = async (_:any, {type, cost, location, time }: DeliveryAttributes, { models }: any) => (
  await models.Delivery.create({
    type,
    time,
    cost,
    location,
  })
);

export default {
  Query: {
    getDelivery,
    
  },
  Mutation: {
    createDelivery,
  }
}