import { ProductAttributes } from "../db/models/product";

export interface Product extends ProductAttributes {
  productTypeId: Number,
}
