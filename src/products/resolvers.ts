import { UserInputError, ApolloError } from 'apollo-server';
import { ID } from '../common';
import { Product } from './product';

const products = async (_: any, args: any, { models }: any) => {
  try {
    return await models.Product.findAll({
      include: [
        { model: models.Category, as: 'category' }
      ]
    });
  } catch (error) {
    throw new ApolloError('There was an error performing your request. Try again later');
  }

};

const getProduct = async (_: any, { id }: ID, { models }: any) => {
  try {
    return await models.Product.findByPk(id, {
      include: [
        { model: models.Category, as: 'category' }
      ]
    });
  } catch (error) {
    throw new ApolloError('Could not find product. Try again later', 'DATABASE ERROR');
  }
};

const createProduct = async (_: any, { name, size, weight, productTypeId }: Product, { models }: any) => {
  try {
    const product = await models.Product.findAll({ where: { name, size } });

    if (product.length) {
      throw new UserInputError(`Product with name ${name} exists`)
    }
    return await models.Product.create({
      name: name.toLowerCase(),
      size,
      weight,
      categoryId: productTypeId
    });
  } catch (error) {
    throw new ApolloError(error);
  }
}

const deleteProduct = async (_: any, { id }: ID, { models }: any) => {
  try {
    return await models.Product.destroy({ where: { id, } });
  } catch (error) {
    throw new ApolloError('Could not delete product. Try again later', 'DATABASE ERROR');
  }

}

const updateProduct = async (_: any, args: Product, { models }: any) => {
  try {
    await models.Product.update({ ...args }, { returning: true, where: { id: args.id } });
    return await models.Product.findByPk(args.id);
  } catch (error) {
    throw new ApolloError('Could not update product. Try again later', 'DATABASE ERROR');
  }
}


export default {
  Query: {
    products,
    getProduct
  },
  Product: {
    category: (category: any) => category.getCategory()
  },
  Mutation: {
    createProduct,
    deleteProduct,
    updateProduct
  }
}