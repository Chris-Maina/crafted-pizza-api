export default 
`
  extend type Query {
    product: Product
    products: [Product]
    getProduct(id: Int): Product
  }
  extend type Mutation {
    createProduct(name: String!, size: String, weight: Int, productTypeId: Int!): Product
    deleteProduct(id: Int): Int
    updateProduct(id: Int, name: String, size: String, weight: Int, productTypeId: Int): Product
  }
  type Product{
    id: Int,
    name: String,
    size: String,
    weight: Int,
    category: ProductType
  }
`;
