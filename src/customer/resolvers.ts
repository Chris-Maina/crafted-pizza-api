import { ID } from "../common";
import { CustomerAttributes } from "../db/models/customer";

const getCustomer = async (_: any, { id }: ID, { models }: any) => ( await models.Customer.findByPk(id));

const createCustomer = async (_: any, { name, phoneNumber, comments }: CustomerAttributes, { models }: any) => (
  await models.Customer.create({
    name,
    comments,
    phoneNumber,
  })
);

export default {
  Query: {
    getCustomer
  },
  Mutation: {
    createCustomer
  }
};
