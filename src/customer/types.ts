export default `
extend type Query {
  getCustomer: Customer
}
extend type Mutation {
  createCustomer(name: String, phoneNumber: String, comments: String,): Customer
}
type Customer {
  id: Int,
  name: String,
  phoneNumber: String,
  comments: String
}

`;
