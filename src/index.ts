/**
 * Register the app here
 */

 import server from './graphql';


 const PORT = process.env.PORT || 4000;
 server.listen(PORT).then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
 