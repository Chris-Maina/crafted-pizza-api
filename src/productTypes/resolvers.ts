import { UserInputError, ApolloError } from 'apollo-server';
import { ID } from '../common';
import { CategoryAttributes } from '../db/models/category';

const productTypes = async (parent: any, arg: any, { models }: any) => {
  try {
    return await models.Category.findAll();
  } catch (error) {
    throw new ApolloError('There was an error getting categories. Try again later');
  } 
}

const createProductType = async (parent: any, { title }: CategoryAttributes, { models }: any) => {
  try {
    const productType = await models.Category.findAll({ where: { title }});
    if (productType.length) {
      throw new UserInputError(`${title} exists. Use another name`);
    }
    return models.Category.create({
      title: title.toLowerCase(),
      key: 'productTypes'
    })
  } catch (error) {
    throw new ApolloError(error);
  }
}

const updateProductType = async (_: any, { id, title}: CategoryAttributes, { models }: any) => {
  try {
    await models.Category.update({ title }, { where: { id }});
    return models.Category.findByPk(id);
  } catch (error) {
    throw new ApolloError(error);
  }
}

const deleteProductType = async (_: any, { id}: ID, {models}: any) => {
  try {
    return await models.Category.destroy({ where: { id, }});
  } catch (error) {
    throw new ApolloError('Could not delete category. Try again later', 'DATABASE ERROR');
  }
}

export default {
  Query: {
    productTypes
  },
  Mutation: {
    createProductType,
    deleteProductType,
    updateProductType,
  }
}
