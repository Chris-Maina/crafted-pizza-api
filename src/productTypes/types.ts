export default 
`
  extend type Query {
    productType: ProductType
    productTypes: [ProductType]
  }
  extend type Mutation{
    deleteProductType(id: Int): Int
    createProductType(title: String): ProductType
    updateProductType(id: Int, title: String): ProductType
  }
  type ProductType {
    id: Int,
    title: String,
    key: String
  }
`;
