import { ApolloError } from 'apollo-server';

const sales = async (_: any, args: any, { models }: any) => {
  try {
    const response = await models.OrderProduct.findAll({
      include: [
        {
          model: models.Product,
        },
        {
          model: models.Order,
        }
      ]
    });
    return response;
  } catch (error) {
    throw new ApolloError('There was an error fetching your sales.');
  }
}

export default {
  Query: {
    sales,
  },
}