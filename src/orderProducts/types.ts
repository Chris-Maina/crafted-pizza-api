export default
`
extend type Query {
  sales: [Sale] 
}

type Sale {
  quantity: Int
  createdAt: Date
  order: Order
  product: Product
}
`