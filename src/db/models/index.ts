/**
 * Combine all the models
 */

import Sequelize from 'sequelize';

import order from './order';
import product from './product';
import delivery from './delivery';
import customer from './customer';
import category from './category';
import DbInterface from './dbInstance';
import orderProduct from './orderProduct';

 const config = require('../config');

 const sequelize = new Sequelize(config['development']);

 const db: DbInterface = {
   sequelize,
   Sequelize,
   Delivery: delivery(sequelize),
   Order: order(sequelize),
   Product: product(sequelize),
   Customer: customer(sequelize),
   Category: category(sequelize),
   OrderProduct: orderProduct(sequelize),
 }

 Object.values(db).forEach((model: any) => {
   if(model.associate) {
    model.associate(db);
   }
 });
 export { sequelize };
 export default db; 
