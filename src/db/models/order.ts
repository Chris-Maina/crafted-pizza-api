import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from './types';

 // Order fields interface
 export interface OrderAttributes {
  id?: number;
  price: string;
  orderedAt: string;
  createdAt?: string;
  updatedAt?: string;
}

// order table row
export type OrderInstance = Sequelize.Instance<OrderAttributes> & OrderAttributes;

// order model
type OrderModel = Sequelize.Model<OrderInstance, OrderAttributes>;

const order = (sequelize: Sequelize.Sequelize): OrderModel => {
  const attributes: SequelizeAttributes<OrderAttributes> = {
    id: {
     primaryKey: true,
     autoIncrement: true,
     type: Sequelize.INTEGER,
    },
    price: {
      type: Sequelize.STRING,
    },
    orderedAt: {
      type: Sequelize.DATEONLY,
    },
  }

  const Order = sequelize.define<OrderInstance, OrderAttributes>("order", attributes);

  /*
   * CASCADE specifies that when a referenced row is deleted, row(s) referencing it should be automatically deleted as well.
   */
  Order.associate = models => {
    Order.belongsTo(models.Customer,  { onDelete: 'CASCADE' }); // Order table is the referencing table and the Customer table is the referenced table
    Order.belongsTo(models.Delivery,  { onDelete: 'CASCADE' }); // Order table is the referencing table and the Delivery table is the referenced table
    Order.belongsToMany(models.Product, { through: models.OrderProduct, as: "products", foreignKey: 'productId' });
  }

  return Order;
}

export default order;