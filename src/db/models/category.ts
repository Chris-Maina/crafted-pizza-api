import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from './types';

export interface CategoryAttributes {
  id?: number;
  title: string;
  key?: string;
  createdAt?: string;
  updatedAt?: string;
}


export type CategoryInstance = Sequelize.Instance<CategoryAttributes> & CategoryAttributes;

type CategoryModel = Sequelize.Model<CategoryInstance, CategoryAttributes>;

const category = (sequelize: Sequelize.Sequelize): CategoryModel => {
  const attributes: SequelizeAttributes<CategoryAttributes> = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
    },
    title: {
      type: Sequelize.STRING,
    },
    key: {
      type: Sequelize.STRING,
    }
  };

  const Category = sequelize.define<CategoryInstance, CategoryAttributes>("category", attributes);

  Category.associate = models => {
    Category.hasMany(models.Product);
  };

  return Category;
}

export default category;