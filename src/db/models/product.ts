import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from './types';

export interface ProductAttributes {
  id?: number;
  name: string;
  size?: string;
  weight?: number;
  createdAt?: string;
  updatedAt?: string;
}

export type ProductInstance = Sequelize.Instance<ProductAttributes> & ProductAttributes;

type ProductModel = Sequelize.Model<ProductInstance, ProductAttributes>;

const product = (sequelize: Sequelize.Sequelize): ProductModel => {
  const attributes: SequelizeAttributes<ProductAttributes> = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
    },
    name: {
     type: Sequelize.STRING,
    },
    size: {
     type: Sequelize.STRING,
   },
   weight: {
     type: Sequelize.INTEGER,
   }
  };

  const Product = sequelize.define<ProductInstance, ProductAttributes>("product", attributes);

  Product.associate = models => {
    Product.belongsTo(models.Category,  { onDelete: 'CASCADE' });
    Product.belongsToMany(models.Order, { through: models.OrderProduct, as: "orders",  foreignKey: 'orderId' });
  };

  return Product;
}

export default product;
