import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from './types';

// Delivery model attributes interface
export interface DeliveryAttributes {
  id?: number;
  type: string;
  location?: string;
  cost?: string;
  time?: string;
  createdAt?: string;
  updatedAt?: string;
}

// sequelize instance of delivery table row
export type DeliveryInstance = Sequelize.Instance<DeliveryAttributes> & DeliveryAttributes;

// sequelize model of Delivery table
type DeliveryModel = Sequelize.Model<DeliveryInstance, DeliveryAttributes>;

const delivery = (sequelize: Sequelize.Sequelize): DeliveryModel => {
  const attributes: SequelizeAttributes<DeliveryAttributes> = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
    },
    type: {
      type: Sequelize.STRING,
    },
    location: {
      type: Sequelize.STRING,
    },
    cost: {
      type: Sequelize.STRING,
    },
    time: {
      type: Sequelize.STRING,
    }
  };

  const Delivery = sequelize.define<DeliveryInstance, DeliveryAttributes>("delivery", attributes);

  Delivery.associate = models => {
    Delivery.hasOne(models.Order);
  };

  return Delivery;
}

export default delivery;