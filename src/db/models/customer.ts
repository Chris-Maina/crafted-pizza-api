import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from './types';

// Customer model attributes/columns
export interface CustomerAttributes {
  id?: string;
  name: string;
  phoneNumber: string;
  comments?: string;
  createdAt?: string;
  updatedAt?: string;
}

// sequelize instance of Customer table row
export type CustomerInstance = Sequelize.Instance<CustomerAttributes> & CustomerAttributes;

// sequelize model for Cutomer table. Return type for the customer function
type CustomerModel = Sequelize.Model<CustomerInstance, CustomerAttributes>;

const customer = (sequelize: Sequelize.Sequelize): CustomerModel => {

  const attributes: SequelizeAttributes<CustomerAttributes> = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
    },
    name: {
      type: Sequelize.STRING
    },
    phoneNumber: {
      type: Sequelize.STRING,
      unique: true
    },
    comments: {
      type: Sequelize.STRING
    },
  }

  const Customer = sequelize.define<CustomerInstance, CustomerAttributes>("customer", attributes);

  Customer.associate = models => {
    Customer.hasMany(models.Order);
  };

  return Customer;
}

export default customer;