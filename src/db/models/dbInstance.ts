import * as Sequelize from 'sequelize';
import { OrderAttributes, OrderInstance } from './order';
import { ProductAttributes, ProductInstance } from './product';
import { CustomerAttributes, CustomerInstance } from './customer';
import { DeliveryInstance, DeliveryAttributes } from './delivery';
import { CategoryAttributes, CategoryInstance } from './category';
import { OrderProductAttributes, OrderProductInstance } from './orderProduct';


export default interface DbInterface {
  sequelize: Sequelize.Sequelize;
  Sequelize: Sequelize.SequelizeStatic;
  Delivery: Sequelize.Model<DeliveryInstance, DeliveryAttributes>;
  Customer: Sequelize.Model<CustomerInstance, CustomerAttributes>;
  Order: Sequelize.Model<OrderInstance, OrderAttributes>,
  Product: Sequelize.Model<ProductInstance, ProductAttributes>,
  Category: Sequelize.Model<CategoryInstance, CategoryAttributes>,
  OrderProduct: Sequelize.Model<OrderProductInstance, OrderProductAttributes>,
 }
