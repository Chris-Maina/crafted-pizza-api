import * as Sequelize from 'sequelize';
import { SequelizeAttributes } from './types';

export interface OrderProductAttributes {
  quantity: number;
  createdAt?: string;
  updatedAt?: string;
}


// orderProduct table row instance
export type OrderProductInstance = Sequelize.Instance<OrderProductAttributes> & OrderProductAttributes;

// orderProduct model
type OrderProductModel = Sequelize.Model<OrderProductInstance, OrderProductAttributes>;

const orderProduct = (sequelize: Sequelize.Sequelize): OrderProductModel => {
  const attributes: SequelizeAttributes<OrderProductAttributes> = {
    quantity: {
     type: Sequelize.INTEGER,
    }
  };

  const OrderProduct = sequelize.define<OrderProductInstance, OrderProductAttributes>("order_product", attributes);

  OrderProduct.associate = models => {
    OrderProduct.belongsTo(models.Order, { onDelete: 'CASCADE'});
    OrderProduct.belongsTo(models.Product, { onDelete: 'CASCADE'});
  }

  return OrderProduct;
}

export default orderProduct;