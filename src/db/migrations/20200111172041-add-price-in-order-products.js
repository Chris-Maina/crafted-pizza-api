"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("products", "price").then(() => {
      return queryInterface.addColumn(
        "orders", // name of Source model
        "price", // key we want to add
        {
          type: Sequelize.STRING
        }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn(
        "products", // name of Source model
        "price", // key we want to add
        {
          type: Sequelize.STRING
        }
      )
      .then(() => {
        return queryInterface.removeColumn(
          "orders", // name of Source model
          "price" // key we want to remove
        );
      });
  }
};
