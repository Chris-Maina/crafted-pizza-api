"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("orders", "orderedAt", {
      type: Sequelize.DATEONLY
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("orders", "orderedAt");
  }
};
