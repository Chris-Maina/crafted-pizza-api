"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint("products", "products_categoryId_fkey", {
        type: Sequelize.INTEGER,
        references: {
          model: "categories", // name of Target model
          key: "id" // key in Target model that we're referencing
        },
        onDelete: "SET NULL",
        onUpdate: "CASCADE"
      })
      .then(() => {
        return queryInterface.addConstraint("products", ["categoryId"], {
          type: "foreign key",
          references: {
            table: "categories", // name of Target model
            field: "id" // field in Target model that we're referencing
          },
          onDelete: "CASCADE",
          onUpdate: "CASCADE"
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint("products", "products_categoryId_categories_fk", {
        type: Sequelize.INTEGER,
        references: {
          model: "categories", // name of Target model
          key: "id" // key in Target model that we're referencing
        },
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
      })
      .then(() => {
        return queryInterface.addConstraint("products", ["categoryId"], {
          type: "foreign key",
          references: {
            table: "categories", // name of Target model
            field: "id" // field in Target model that we're referencing
          },
          onDelete: "SET NULL",
          onUpdate: "CASCADE"
        });
      });
  }
};
