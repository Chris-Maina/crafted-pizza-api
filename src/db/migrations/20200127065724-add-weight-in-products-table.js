'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "products", // name of Source model
      "weight", // key we want to add
      {
        type: Sequelize.INTEGER,
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      "products", // name of Source model
      "weight" // key we want to remove
    );
  }
};
