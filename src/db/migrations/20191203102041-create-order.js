'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("orders",
    {
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,
       },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("orders");
  }
};
