'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    // Order belongs to Customer
    return queryInterface.addColumn(
      'orders', // name of Source model
      'customerId', // key we want to add
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'customers',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    ).then(() => {
      // Delivery has one order
      return queryInterface.addColumn(
        'orders',
        'deliveryId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'deliveries',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        }
      )
    }).then(() => {
      // Product belongs to a category
      return queryInterface.addColumn(
        'products',
        'categoryId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'categories',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        }
      )
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'orders', // name of Source model
      'customerId' // key we want to remove
    ).then(() => {
      return queryInterface.removeColumn(
        'orders',
        'deliveryId',
      )
    }).then(() => {
      return queryInterface.removeColumn(
        'products',
        'categoryId',
      )
    })
  }
};
