'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("products",
    {
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,
      },
      name: {
       type: Sequelize.STRING,
      },
      price: {
        type: Sequelize.STRING,
      },
      size: {
       type: Sequelize.STRING,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("products");
  }
};
