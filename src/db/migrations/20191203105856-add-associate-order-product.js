'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("order_products",
    {
      quantity: {
       type: Sequelize.INTEGER,
      },
      productId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      orderId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,

    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("order_products");
  }
};
