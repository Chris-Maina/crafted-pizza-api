"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .changeColumn("order_products", "productId", {
        type: Sequelize.INTEGER,
        primaryKey: false,
        allowNull: false
      })
      .then(() => {
        return queryInterface
          .changeColumn("order_products", "orderId", {
            type: Sequelize.INTEGER,
            primaryKey: false,
            allowNull: false
          })
          .then(() => {
            return queryInterface.addColumn(
              "order_products", // name of Source model
              "id", // key we want to add
              {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              }
            );
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeColumn(
        "order_products", // name of Source model
        "id" // key we want to remove
      )
      .then(() => {
        return queryInterface
          .changeColumn("order_products", "productId", {
            type: Sequelize.INTEGER,
            primaryKey: true
          })
          .then(() => {
            return queryInterface
              .changeColumn("order_products", "orderId", {
                type: Sequelize.INTEGER,
                primaryKey: true
              })
          });
      });
  }
};
