'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint("orders", "orders_deliveryId_fkey", {
        type: Sequelize.INTEGER,
        references: {
          model: "deliveries", // name of Target model
          key: "id" // key in Target model that we're referencing
        },
        onDelete: "SET NULL",
        onUpdate: "CASCADE"
      })
      .then(() => {
        return queryInterface.addConstraint("orders", ["deliveryId"], {
          type: "foreign key",
          name: "orders_deliveryId_fkey",
          references: {
            table: "deliveries", // name of Target model
            field: "id" // field in Target model that we're referencing
          },
          onDelete: "CASCADE",
          onUpdate: "CASCADE"
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    // Remove the update applied i.e.  onDelete: "CASCADE"
    return queryInterface
      .removeConstraint("orders", "orders_deliveryId_fkey", {
        type: Sequelize.INTEGER,
        references: {
          model: "deliveries", // name of Target model
          key: "id" // key in Target model that we're referencing
        },
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
      })
      .then(() => {
        // Add the old constraint i.e. onDelete: "SET NULL"
        return queryInterface.addConstraint("products", ["deliveryId"], {
          type: "foreign key",
          name: "orders_deliveryId_fkey",
          references: {
            table: "deliveries", // name of Target model
            field: "id" // field in Target model that we're referencing
          },
          onDelete: "SET NULL",
          onUpdate: "CASCADE"
        });
      });
  }
};
