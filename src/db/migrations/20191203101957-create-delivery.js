'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("deliveries",
    {
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
      id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,
      },
      type: {
       type: Sequelize.STRING,
      },
      location: {
        type: Sequelize.STRING,
      },
      cost: {
       type: Sequelize.STRING,
      },
      time: {
        type: Sequelize.STRING,
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("deliveries");
  }
};
