/**
 * Import reesolvers and mutationa
 * Make them executable
 */

 import { ApolloServer } from 'apollo-server';

 import schema from './schema';
 import models from './db/models';

 export default new ApolloServer({ schema, context: { models } });
