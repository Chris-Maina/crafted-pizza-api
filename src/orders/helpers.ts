import { Op } from 'sequelize';
import { CustomerAttributes } from '../db/models/customer';
import { DeliveryAttributes } from '../db/models/delivery';

const getCustomer = async (customerData: CustomerAttributes, models: any) => {
  let customer: any = null;
  customer = await models.Customer.findOne({
    where: {
      phoneNumber: {
        [Op.like]: customerData.phoneNumber
      }
    }
  });
  if(!customer) {
    customer = await models.Customer.create({ ...customerData });
  }
  return customer;
}

const getDelivery = async (deliveryData: DeliveryAttributes, models: any) => {
  let delivery: any = null;
  delivery = await models.Delivery.findOne({
    where: {
      location: {
        [Op.like]: deliveryData.location
      }
    }
  });
  if(!delivery) {
    delivery = await models.Delivery.create({ ...deliveryData });
  }
  return delivery;
}

export { getCustomer, getDelivery };
