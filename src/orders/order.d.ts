import { CustomerAttributes } from "../db/models/customer";
import { DeliveryAttributes } from "../db/models/delivery";

export interface OrderProduct {
  productId: Number,
  quantity: Number,
}

export interface Order {
  price: Number,
  id?: Number,
  orderedAt: Date,
  customer: CustomerAttributes,
  delivery: DeliveryAttributes,
  customerOrders: OrderProduct[],
}
