import { ApolloError } from 'apollo-server';
import { ID } from '../common';
import { OrderProduct, Order } from './order';
import { getCustomer, getDelivery } from './helpers';

const orders = async (_: any, args: any, { models }: any) => {
  try {
    const response = await models.Order.findAll({
      include: [
        {
          model: models.Product,
          as: 'products',
          through: {
            model: models.OrderProduct,
            as: 'quantity',
            attributes: ['quantity'],
          }
        },
        {
          model: models.Customer,
          as: 'customer',
        },
        {
          model: models.Delivery,
          as: 'delivery',
        }
      ],
      order: [
        ['id', 'DESC'],
      ],
    });
    return response;
  } catch (error) {
    console.log('>>>>>>>>>>> error', error)
    throw new ApolloError('There was an error fetching your orders.');
  }
}


const saveOrder = async (_: any, { price, orderedAt, customer, delivery, customerOrders }: Order, { models }: any) => {
  try {
    const createdCustomer = await getCustomer(customer, models);
    const createdDelivery = await getDelivery(delivery, models);

    const order = await models.Order.create({
      price,
      orderedAt,
      customerId: createdCustomer.id,
      deliveryId: createdDelivery.id,
    });

    try {
      await Promise.all(customerOrders.map(async (item: OrderProduct) => {
        // Search for the product with the givenId and make sure it exists. If it doesn't, throw error.
        const product = await models.Product.findByPk(item.productId);
        if (!product) {
          throw new ApolloError(`Could not find product with id ${item.productId}`);
        }
  
        // Insert in join table
        return await order.addProduct(item.productId, { through: { quantity: item.quantity } });
  
      }));
    } catch (error) {
      throw new ApolloError('There was an error adding ordered products. Try again.');
    }

    return await models.Order.findOne({
      where: { id: order.id },
      include: [
        {
          model: models.Product,
          as: 'products',
          through: {
            model: models.OrderProduct,
            as: 'quantity',
            attributes: ['quantity'],
          }
        },
        {
          model: models.Customer,
          as: 'customer',
        },
        {
          model: models.Delivery,
          as: 'delivery',
        }
      ]
    })
  } catch (error) {
    throw new ApolloError('There was an error saving your sale. Try again.');
  }
}

const deleteOrder = async (_: any, { id }: ID, { models }: any) => {
  const order = await models.Order.findByPk(id);

  // Find and remove all associations 
  const products = await order.getProducts();
  order.removeProducts(products);

  return await models.Order.destroy({ where: { id, } });
}

const updateOrder = async (_: any, { id, price, orderedAt, customer, delivery, customerOrders }: Order, { models }: any) => {
  await models.Customer.update({ ...customer }, { where: { id: customer.id } });

  await models.Delivery.update({ ...delivery }, { where: { id: delivery.id } });

  // Update the price Property
  await models.Order.update({ price, orderedAt }, { where: { id } });


  const order = await models.Order.findByPk(id);
  // Find and remove all associations 
  const products = await order.getProducts();
  await order.removeProducts(products);

  try {
    // Loop through all the customer orders in the request & create them
    await Promise.all(customerOrders.map(async (item: OrderProduct) => {
      // Search for the product with the givenId and make sure it does not exists. If it doesn't, throw error.
      const product = await models.Product.findByPk(item.productId);
      if (!product) {
        throw new ApolloError(`Could not find product with id ${item.productId}`);
      }

      // Insert in join table
      return await order.addProduct(item.productId, { through: { quantity: item.quantity } });
    }));
  } catch (error) {
    throw new ApolloError('There was an error updating ordered products. Try again.');
  }

  // Find the order
  return await models.Order.findByPk(id, {
    include: [
      {
        model: models.Product,
        as: 'products',
        through: {
          model: models.OrderProduct,
          as: 'quantity',
          attributes: ['quantity'],
        }
      },
      {
        model: models.Customer,
        as: 'customer',
      },
      {
        model: models.Delivery,
        as: 'delivery',
      }
    ]
  });
}

const getOrder = async (_: any, { id }: ID, { models }: any) => {
  try {
    const order = await models.Order.findByPk(id, {
      include: [
        {
          model: models.Product,
          as: 'products',
          through: {
            model: models.OrderProduct,
            as: 'quantity',
            attributes: ['quantity'],
          }
        },
        {
          model: models.Customer,
          as: 'customer',
        },
        {
          model: models.Delivery,
          as: 'delivery',
        }
      ]
    });
    if (!order) throw new ApolloError('The selected order does not exist');
    return order;
  } catch (error) {
    throw new ApolloError(error);
  }

}

export default {
  Query: {
    orders,
    order: getOrder
  },
  Mutation: {
    saveOrder,
    deleteOrder,
    updateOrder,
  },
  OrderProduct: {
    quantity: ({ quantity }: any) => quantity.quantity
  }
};
