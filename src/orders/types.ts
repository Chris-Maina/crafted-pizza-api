export default  
`
  scalar Date
  
  extend type Query {
    hello: Greeting
    orders: [Order]
    order(id: Int): Order
  }
  extend type Mutation {
    saveOrder(price: Int, orderedAt: Date, customer: CustomerInput, delivery: DeliveryInOrderInput, customerOrders: [CustomerOrderInput]): Order
    deleteOrder(id: Int): Int
    updateOrder(id: Int, price: Int, orderedAt: Date, customer: CustomerInput, delivery: DeliveryInOrderInput, customerOrders: [CustomerOrderInput]): Order
  }
  type Greeting {
    greeting: String
  }
  type Order {
    id: Int,
    price: Int,
    orderedAt: Date,
    createdAt: Date,
    customer: Customer,
    delivery: Delivery,
    products: [OrderProduct]
  }
  type OrderProduct {
    id: Int,
    name: String,
    size: String,
    quantity: Int
  }

  input CustomerInput {
    id: Int,
    name: String,
    phoneNumber: String,
    comments: String
  }
  input DeliveryInOrderInput {
    id: Int,
    cost: Int,
    type: String!,
    location: String,
  }
  input CustomerOrderInput {
    productId: Int
    quantity: Int
  }
`;