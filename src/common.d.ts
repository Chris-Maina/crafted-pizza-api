import DbInterface from "./db/models/dbInstance";

export interface ID {
  id: Number,
}

export interface Context {
  models: DbInterface,
}
