import { makeExecutableSchema } from 'graphql-tools';

import { Delivery, deliverResolvers } from './delivery';
import { Product, productResolvers } from './products';
import { orderResolvers, Greeting } from './orders';
import { customerResolvers, Customer } from './customer';
import { orderProductResolvers, OrderProduct } from './orderProducts';
import { ProductType, productTypeResolvers } from './productTypes';

  const Query = `
    type Query  {
      _empty: String
    }
    type Mutation {
      _empty: String
    }
  `;

  const resolvers = {}

export default makeExecutableSchema(
  { 
    typeDefs: [ Query, Greeting, Customer, Product, Delivery, ProductType, OrderProduct ],
    resolvers: [
      resolvers,
      orderResolvers,
      productResolvers,
      deliverResolvers,
      customerResolvers,
      productTypeResolvers,
      orderProductResolvers,
    ]
  });
